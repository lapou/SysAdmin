import requests

import speedtest

headers = {
    'Content-type': 'application/json',
}

def humansize(nbytes):
    suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']
    i = 0
    while nbytes >= 1024 and i < len(suffixes)-1:
        nbytes /= 1024.
        i += 1
    f = ('%.2f' % nbytes).rstrip('0').rstrip('.')
    return '%s %s' % (f, suffixes[i])

# Speed test
st = speedtest.Speedtest()

# Upload speed
us = st.upload()

#print(us)

# Download Speed
ds = st.download()

x = humansize(us)


y = humansize(ds)

print(x)

print(y)


s = y
characters = " MB"

for x in range(len(characters)):
    s = s.replace(characters[x],"")

print(s)

t = float(s)

if t < 15.04:

    data = '{"text":"connexion faible"}'

    response = requests.post('https://hooks.slack.com/services/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', headers=headers, data=data)

else:

    data = '{"text":"connexion normale"}'

    response = requests.post('https://hooks.slack.com/services/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', headers=headers, data=data)

