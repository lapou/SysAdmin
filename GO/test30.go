package main

import (
	"fmt"
)

func main() {

	const (
		maxligne   int = 3
		maxcolonne int = 4
	)

	var tableau [maxligne][maxcolonne]int

	fmt.Println(tableau)

	fmt.Println("-------------------------------")

	tableau[1][2] = 4

	fmt.Println(tableau)
}
