package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"
)

func main() {

	scanner := bufio.NewScanner(os.Stdin) // création du scanner capturant une entrée utilisateur

	fmt.Print("Entrez votre age : ")

	scanner.Scan() // lancement du scanner

	age, err := strconv.Atoi(scanner.Text())

	if err != nil {
		fmt.Println("Merci de saisir un nombre!!!")

		os.Exit(2)
	}

	fmt.Println("Entrez votre prenom : ")
	scanner.Scan()
	prenom := scanner.Text()

	/*
	   On a besoin de changer la graine (générateur de nombres pseudo-aléatoires) à
	   chaque exécution de programmation sinon on obtiendra le même nombre aléatoire
	*/
	rand.Seed(time.Now().UnixNano())
	radomInt := rand.Intn(2) // retourne aléatoirement soit 1 soit 0
	radomInt2 := rand.Intn(2)

	if age < 18 {

		fmt.Println("Dehors!!!")
	} else if prenom == "dioulde" || prenom == "Dioulde" {

		fmt.Println("Je reconnais ton nom sors!!!")

	} else if age == 18 && radomInt == 1 {

		fmt.Println("Vous avez de la chance que vous me plaisez!!!")

	} else if radomInt2 == 0 {

		fmt.Println("Sortez ce n'est pas votre jour de chance!!!")
	} else {
		fmt.Println("Entrez!!!!!!!!")
	}
}
