package main

import (
	"fmt"
)

func main() {
	var x int = 20
	var ap *int
	ap = &x

	fmt.Printf("Adresse de notre variable : %p\n", &x)

	fmt.Printf("Valeur de notre (pointeur) ap : %p\n", ap)

	fmt.Printf("Valeur de l'adresse %p: %d\n", ap, *ap)

}
