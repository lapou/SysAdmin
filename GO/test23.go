package main

import (
	"fmt"
)

var x int

func test() {
	x += 15
	fmt.Println("dans ma fonction ", x)
}

func main() {

	fmt.Println("Avant l'utilisation de ma focntion test ", x)

	test()

	fmt.Println("En dehors de ma fonction ", x)

	x += 10
	fmt.Println("modifie ma valeur ", x)

}
