package main

import (
	"fmt"
)

func main() {

	var ptr *int

	if ptr == nil {

		fmt.Println("Aucune adresse memoire")
	} else {
		fmt.Printf("Votre adresse memoire est %p\n", &ptr)
	}

	var x int = 40

	var px *int

	px = &x

	if px == nil {

		fmt.Println("Aucune adresse memoire")
	} else {

		fmt.Printf("L'adreese de votre pointeur est %p\n", px)

	}
}
