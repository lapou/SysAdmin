package main

import (
	"fmt"
)

func maxNbre(a int, b int) int {
	if a > b {
		return a
	}
	return b
}

func main() {
	max := maxNbre(10, 30)
	fmt.Println(max)

	fmt.Printf("valeur : %d , Type : %T\n", maxNbre(10, 30), maxNbre(10, 30))
}
