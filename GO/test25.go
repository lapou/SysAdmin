package main

import (
	"fmt"
)

func main() {
	var jours = [7]string{"lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"}

	fmt.Println("jour[0] =", jours[0])
	fmt.Println("jour[1] =", jours[1])
	fmt.Println("jour[2] =", jours[2])
	fmt.Println("jour[3] =", jours[3])
	fmt.Println("jour[4] =", jours[4])
	fmt.Println("jour[5] =", jours[5])
	fmt.Println("jour[6] =", jours[6])
}
