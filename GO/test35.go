package main

import (
	"fmt"
)

func main() {
	type Personnage struct {
		nom string
		age int
	}

	//var perso Personnage

	perso := Personnage{"toto", 35}

	fmt.Println(perso)

	fmt.Println("Je veux connaitre juste l'age ==> ", perso.age)

	perso.age = 40

	fmt.Println("Mec t'as vieilli hein ", perso.age)
}
