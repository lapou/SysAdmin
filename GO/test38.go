package main

import (
	"fmt"
)

type Personnage struct {
	nom        string
	vie        int
	puissance  int
	mort       bool
	inventaire [3]string
}

func main() {

	var p1 Personnage
	var p2 Personnage

	valeurParDefaut(&p1)
	valeurParDefaut(&p2)

	p1.nom = "barbare"
	p2.nom = "magicien"

	p1.affichage()
	p2.affichage()

}

func (p Personnage) affichage() {
	fmt.Println("----------------------------------------")
	fmt.Println("Vie du personnage :", p.nom, ":", p.vie)
	fmt.Println("Puissance du personnage :", p.nom, ":", p.puissance)

	if p.mort {
		fmt.Println("Vie du personnage :", p.nom, "est mort")
	} else {
		fmt.Println("Vie du personnage :", p.nom, "est vivant")
	}

	fmt.Println("Le personnage :", p.nom, "possède dans son inventaire :", p.vie)

	for _, objet := range p.inventaire {
		fmt.Println("-", objet)
	}
}

func valeurParDefaut(p1 *Personnage) {

	p1.nom = "inconnu"
	p1.vie = 10
	p1.puissance = 10
	p1.mort = true
	p1.inventaire = [3]string{"vide", "vide", "vide"}
}
