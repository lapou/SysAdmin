package main

import (
	"fmt"
)

func main() {
	a := 4
	b := 7
	fmt.Println("Avant appel focntion a =", a, "b =", b)
	a, b = Addition(a, b)
	fmt.Println("Après appel focntion a =", a, "b =", b)
}
func Addition(a int, b int) (int, int) {
	return a + 3, b + 3
}
