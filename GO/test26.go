package main

import (
	"fmt"
)

func main() {
	var jours = [7]string{"lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"}

	for index, jour := range jours {

		fmt.Println(jour, "est le jour numéro", (index + 1), "de la semaine")

	}
}
