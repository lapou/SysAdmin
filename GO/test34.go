package main

import (
	"fmt"
)

func main() {

	var a int = 10

	var b *int

	b = &a

	fmt.Printf("Valeur de la variable a %d\n", a)

	*b = 20

	fmt.Printf("Valeur de la variable a %d\n", a)

}
