package main

import (
	"fmt"
)

func main() {
	notes := map[string]int{"Aziz": 20, "Taha": 15, "Niang": 14}

	fmt.Println(notes)

	delete(notes, "Niang")

	fmt.Println(notes)

}
