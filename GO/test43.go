package main

import (
	"fmt"
)

func main() {

	notes := map[string]int{"Aziz": 20, "Taha": 15, "Niang": 14}

	fmt.Println(notes)
	fmt.Println("La note de Aziz est :", notes["Aziz"])

	notes["Habi"] = 13
	notes["Amour"] = 12
	fmt.Println(notes)

}
