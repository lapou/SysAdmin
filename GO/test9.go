package main

import "fmt"

func main() {

	var a int = 4

	a++ //incrmentation

	fmt.Println("incrementation de 1 :", a)

	a-- //decrementation

	fmt.Println("decrementation de 1 :", a)
}
