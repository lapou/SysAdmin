package main

import (
	"fmt"
)

type Figure interface {
	Air() float64
	Perimetre() float64
}

type Rectangle struct {
	Longueure float64
	Largeur   float64
}

func (r Rectangle) Air() float64 {

	return r.Largeur * r.Longueure

}

func (r Rectangle) Perimetre() float64 {

	return 2 * (r.Longueure + r.Largeur)
}

func main() {
	var f Figure
	f = Rectangle{5.0, 4.0} // affectation de la structure Rectangle à l'interface Forme
	r := Rectangle{5.0, 4.0}
	fmt.Println("Type de f :", f)
	fmt.Printf("Valeur de f : %v\n", f)
	fmt.Println("Air du rectangle r :", f.Air())
	fmt.Println("Perimetre du rectangle r :", f.Perimetre())
	fmt.Println("f == r ? ", f == r)
}
