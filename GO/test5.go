package main

import "fmt"

func main() {
	/*
	   Déclaration des variables dynamiques
	*/
	flt := 15.5   //  sera automatiquement de type float
	in := 5       //  sera automatiquement de type int
	st := "hello" //  sera automatiquement de type string
	bol := true   //  sera automatiquement de type boolean

	fmt.Printf("La valeur de la varialbe flt est %f\n", flt)
	fmt.Printf("La valeur de la varialbe in est %d\n", in)
	fmt.Printf("La valeur de la varialbe st est %s\n", st)
	fmt.Printf("Le type de la varialbe bol est %T\n", bol)

}
