package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func division() {
	scanner := bufio.NewScanner(os.Stdin)

	//Input de l'utilisateur

	fmt.Println("Veuillez saisir un chiffre :")
	scanner.Scan()

	// Conversion de linput en chaine de caractère
	chiffre, err := strconv.Atoi(scanner.Text())

	// S'assurer que le user saisit un nombre et non une chaine de caractère
	if err != nil {
		fmt.Println("Veuillez saisir un nombre pas une chaine de caractère!!!!")
	}
	// S'assurer que le user ne saisit pas le nombre 0.
	if chiffre <= 0 {
		fmt.Println("Impossible de diviser par zero, veuillez saisir un nombre different de zero!!!")
	} else {
		fmt.Println("Resultat :", 1000/chiffre)

	}
}

func main() {
	division()
	fmt.Println("Fin de l'operation!!!")

}
