package main

import (
	"fmt"
)

type Personnage struct {
	nom        string
	vie        int
	puissance  int
	mort       bool
	inventaire [3]string
}

func main() {

	var p1 Personnage

	valeurParDefaut(&p1)

	fmt.Println("Vie du personnage :", p1.nom, ":", p1.vie)
	fmt.Println("Puissance du personnage :", p1.nom, ":", p1.puissance)

	if p1.mort {
		fmt.Println("Vie du personnage :", p1.nom, "est mort")
	} else {
		fmt.Println("Vie du personnage :", p1.nom, "est vivant")
	}

	fmt.Println("Le personnage :", p1.nom, "possède dans son inventaire :", p1.vie)

	for _, objet := range p1.inventaire {
		fmt.Println("-", objet)
	}
}

func valeurParDefaut(p1 *Personnage) {

	p1.nom = "inconnu"
	p1.vie = 10
	p1.puissance = 10
	p1.mort = true
	p1.inventaire = [3]string{"vide", "vide", "vide"}
}
