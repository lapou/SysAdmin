package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	scan := bufio.NewScanner(os.Stdin)
	fmt.Print("Veuillez entrer un nombre entier : ")
	scan.Scan()
	nombre, _ := strconv.Atoi(scan.Text())
	fmt.Printf("res : %d\n", nombre)

}
