package main

import (
	"fmt"
)

type Personnage struct {
	nom        string
	vie        int
	puissance  int
	mort       bool
	inventaire [3]string
}

func main() {

	var p1 Personnage

	p1.nom = "diallo"
	p1.vie = 100
	p1.puissance = 30
	p1.mort = false
	p1.inventaire = [3]string{"potion", "baton", "poison"}

	fmt.Println("Vie du personnage :", p1.nom, ":", p1.vie)
	fmt.Println("Puissance du personnage :", p1.nom, ":", p1.puissance)

	if p1.mort {
		fmt.Println("Vie du personnage :", p1.nom, "est mort")
	} else {
		fmt.Println("Vie du personnage :", p1.nom, "est vivant")
	}

	fmt.Println("Le personnage :", p1.nom, "possède dans son inventaire :", p1.vie)

	for _, objet := range p1.inventaire {
		fmt.Println("-", objet)
	}

}
