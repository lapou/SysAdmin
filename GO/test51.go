package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func main() {

	file, err := os.OpenFile("/home/aziz/fichego.txt", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 644)
	defer file.Close()

	if err != nil {
		panic(err)
	}

	_, err = file.WriteString("salam aziz\n")
	if err != nil {
		panic(err)
	}

	_, err = file.WriteString("comment allez-vous?\n")
	if err != nil {
		panic(err)
	}

	data, err := ioutil.ReadFile("/home/aziz/fichego.txt")
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf(string(data))
	}
}
