package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"
)

func main() {

	rand.Seed(time.Now().UnixNano())
	randomInt := rand.Intn(10)

	scanner := bufio.NewScanner(os.Stdin)

	max := 20

	for true {
		fmt.Print("Entrez un nombre : ")
		scanner.Scan()

		nbre, err := strconv.Atoi(scanner.Text())

		if err != nil {
			fmt.Print("Entrez un nombre : ")
			continue

		}
		if nbre > max || nbre < 0 {
			fmt.Println("Veuillez saisir un nombre entre 0, et", max, "!!!")
			continue

		} else if nbre == randomInt {
			fmt.Println("Bien joué!!!")
		} else {
			fmt.Println("Dommage vous avez perdu!!!")
		}
	}

}
