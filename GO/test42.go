package main

import (
	"fmt"
)

func main() {

	animaux1 := []string{"lions", "cheval", "loup"}

	fmt.Println("Contenu du tableau animaux1 :", animaux1)

	animaux2 := make([]string, len(animaux1))

	copy(animaux2, animaux1)

	fmt.Println("Contenu du tableau animaux2 :", animaux2)

}
