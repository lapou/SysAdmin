package main

import (
	"fmt"
	"math"
)

type Figure interface {
	Air() float64
	Perimetre() float64
}

type Rectangle struct {
	Longueure float64
	Largeur   float64
}

func (r Rectangle) Air() float64 {

	return r.Largeur * r.Longueure

}

func (r Rectangle) Perimetre() float64 {

	return 2 * (r.Longueure + r.Largeur)
}

type Cercle struct {
	rayon float64
}

func (c Cercle) Air() float64 {
	return c.rayon * c.rayon * math.Pi
}

func (c Cercle) Perimetre() float64 {
	return 2 * c.rayon * math.Pi
}

func AirPerimetrePresentation(f Figure) {
	fmt.Println("- Air :", f.Air())
	fmt.Println("- Perimetre :", f.Perimetre())
}

func main() {
	var r Figure = Rectangle{5.0, 4.0}
	var c Figure = Cercle{5.0}

	fmt.Println("Cercle :")
	AirPerimetrePresentation(r)
	fmt.Println("\nrectangle :")
	AirPerimetrePresentation(c)
}
