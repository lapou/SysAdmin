package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Print("votre choix : ")
	scanner.Scan()
	choix, err := strconv.Atoi(scanner.Text())

	if err != nil {
		fmt.Println("Veuillez saisir un nombre :")
		os.Exit(2)
	}

	switch choix {
	case 0, 1:
		fmt.Println("Afrique")
	case 2:
		fmt.Println("Amerique")
	case 4:
		fmt.Println("Asie")
	case 6:
		fmt.Println("Europe")
	case 8:
		fmt.Println("Oceanie")
	default:
		fmt.Println("Ce continent n'existe pas sur terre!!!")
	}
}
