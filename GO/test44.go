package main

import (
	"fmt"
)

func main() {
	notes := map[string]int{"Aziz": 20, "Taha": 15, "Niang": 14}

	for etudiant := range notes {

		fmt.Println("La note de ", etudiant, "est :", notes[etudiant])

	}

}
