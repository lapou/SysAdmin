package main

import "fmt"

func main() {

	var (
		a int = 5

		b int = 2
	)

	fmt.Println(" a + b =", a+b)
	fmt.Println(" a - b =", a-b)
	fmt.Println(" a * b =", a*b)
	fmt.Println(" a % b =", a%b)

}
